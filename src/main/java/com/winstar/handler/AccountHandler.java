package com.winstar.handler;

import com.winstar.domain.Account;
import com.winstar.repository.AccountRepository;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;

/**
 * account handler
 *
 * @author gradle
 */
public class AccountHandler {
    private final AccountRepository repository;

    public AccountHandler(AccountRepository repository) {
        this.repository = repository;
    }

    public Mono<ServerResponse> getAccount(ServerRequest request) {
        int accountId = Integer.valueOf(request.pathVariable("id"));
        Mono<ServerResponse> notFound = ServerResponse.notFound().build();
        Mono<Account> accountMono = this.repository.getById(accountId);
        return accountMono.flatMap(account -> ServerResponse.ok().contentType(APPLICATION_JSON_UTF8).body(fromObject(account))).switchIfEmpty(notFound);
    }

    public Mono<ServerResponse> createAccount(ServerRequest request) {
        Mono<Account> account = request.bodyToMono(Account.class);
        return ServerResponse.ok().build(this.repository.save(account));
    }

    public Mono<ServerResponse> listAccount(ServerRequest request) {
        Flux<Account> accountList = this.repository.getAll();
        return ServerResponse.ok().contentType(APPLICATION_JSON_UTF8).body(accountList, Account.class);
    }
}
