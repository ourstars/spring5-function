package com.winstar;

import com.winstar.handler.AccountHandler;
import com.winstar.repository.AccountRepository;
import com.winstar.repository.AccountRepositoryImpl;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ServletHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.RouterFunctions.toHttpHandler;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

/**
 * @author gradle 2017-07-10 15:20:47
 * @version v1.0.0
 */
public class Server {
    private static final String HOST = "localhost";
    private static final int PORT = 8080;

    public static void main(String[] args) throws Exception {
        Server server = new Server();
        server.startTomcatServer();
        System.out.println("Press ENTER to exit.");
        System.in.read();
    }

    private void startTomcatServer() throws LifecycleException {
        RouterFunction<?> route = routingFunction();
        HttpHandler httpHandler = toHttpHandler(route);

        Tomcat tomcatServer = new Tomcat();
        tomcatServer.setHostname(HOST);
        tomcatServer.setPort(PORT);
        Context rootContext = tomcatServer.addContext("", System.getProperty("java.io.tmpdir"));
        ServletHttpHandlerAdapter servlet = new ServletHttpHandlerAdapter(httpHandler);
        Tomcat.addServlet(rootContext, "httpHandlerServlet", servlet);
        rootContext.addServletMapping("/", "httpHandlerServlet");
        tomcatServer.start();
    }


    private RouterFunction<ServerResponse> routingFunction() {
        AccountRepository repository = new AccountRepositoryImpl();
        AccountHandler handler = new AccountHandler(repository);
        return route(GET("/account/{id}"), handler::getAccount)
                .and(route(GET("/account"), handler::listAccount))
                .and(route(POST("/account"), handler::createAccount));
    }
}
