package com.winstar.repository;

import com.winstar.domain.Account;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * account Repository implement
 *
 * @author gradle
 */
public class AccountRepositoryImpl implements AccountRepository {
    private final Map<Integer, Account> accountList = new HashMap<>();

    public AccountRepositoryImpl() {
        this.accountList.put(1, new Account("王小虎", 12));
        this.accountList.put(2, new Account("邵育才", 30));
    }

    @Override
    public Mono<Account> getById(int id) {
        return Mono.justOrEmpty(this.accountList.get(id));
    }

    @Override
    public Flux<Account> getAll() {
        return Flux.fromIterable(this.accountList.values());
    }

    @Override
    public Mono<Void> save(Mono<Account> accountMono) {
        return accountMono.doOnNext(account -> {
            int id = accountList.size() + 1;
            accountList.put(id, account);
            System.out.format("Saved %s with id %d%n", account, id);
        }).thenEmpty(Mono.empty());
    }
}
