package com.winstar.repository;

import com.winstar.domain.Account;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * account repository
 *
 * @author gradle
 */
public interface AccountRepository {

    Mono<Account> getById(int id);

    Flux<Account> getAll();

    Mono<Void> save(Mono<Account> account);

}
